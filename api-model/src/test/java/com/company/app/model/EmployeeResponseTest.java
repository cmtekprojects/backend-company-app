package com.company.app.model;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = EmployeeResponseTest.class)
@JsonTest
public class EmployeeResponseTest {

    @Test
    public void validateEmployeeResponse() {
        final String employees = "{\"employees\":[\n" +
                "{\"name\": \"Pep\",\"lastEventDate\": \"1985-04-23\"},\n" +
                "{\"name\": \"Joana\",\"lastEventDate\": \"2015-08-15\"},\n" +
                "{\"name\": \"Nina\",\"lastEventDate\": \"2012-12-12\"},\n" +
                "{\"name\": \"Miquel\",\"lastEventDate\": \"1999-09-19\"},\n" +
                "{\"name\": \"Rosa\"}\n" +
                "]}";

        final EmployeeResponse employeeResponse = EmployeeResponse.builder()
                .employees(employees)
                .build();

        assertThat(employeeResponse.getEmployees(), is(employees));
    }
}
