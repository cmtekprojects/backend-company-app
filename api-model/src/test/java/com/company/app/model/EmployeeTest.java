package com.company.app.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = EmployeeTest.class)
@JsonTest
public class EmployeeTest {

    @Autowired
    private JacksonTester<Employee> json;
    private static final String NAME = "Rocio";
    private static final String LAST_EVENT_DATE = "2019-05-19";


    private static final Employee EMPLOYEE_RESPONSE = Employee.builder()
            .name(NAME)
            .lastEventDate(LAST_EVENT_DATE)
            .build();

    private static final String JSON_EMPLOYEE = "{\n" +
            "    \"name\": \"Rocio\",\n" +
            "    \"lastEventDate\": \"2019-05-19\"       \n" +
            "}";

    @Test
    public void shouldSerializeProperly() throws IOException {
        assertThat(this.json.write(EMPLOYEE_RESPONSE)).isEqualTo(JSON_EMPLOYEE);
    }

    @Test
    public void shouldDeserializeProperly() throws IOException {
        assertThat(this.json.parse(JSON_EMPLOYEE)).isEqualTo(EMPLOYEE_RESPONSE);
    }
}
