package com.company.app.model;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import java.io.Serializable;

@JsonDeserialize(builder = EmployeeResponse.EmployeeResponseBuilder.class)
@Data
@Builder
@Validated
public class EmployeeResponse implements Serializable {

    private static final long serialVersionUID = -3982976756010034412L;
    @JsonRawValue
    private final String employees;

    private EmployeeResponse(final EmployeeResponseBuilder builder) {
        employees = builder.employees;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class EmployeeResponseBuilder {
        public EmployeeResponse build() {
            return new EmployeeResponse(this);
        }
    }
}
