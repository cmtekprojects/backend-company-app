package com.company.app.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import java.io.Serializable;

@JsonDeserialize(builder = Employee.EmployeeBuilder.class)
@Data
@Builder
@Validated
public class Employee implements Serializable {

    private static final long serialVersionUID = -3982976756010034412L;
    private final String name;
    private final String lastEventDate;

    private Employee(final EmployeeBuilder builder) {
        this.name = builder.name;
        this.lastEventDate = builder.lastEventDate;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class EmployeeBuilder {
        public Employee build() {
            return new Employee(this);
        }
    }
}
