package com.company.controller;

import com.company.AppApplication;
import com.company.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = EmployeeController.class)
@ContextConfiguration(classes = AppApplication.class)
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private EmployeeService employeeService;

    @Test
    public void employeeListShouldBeOrderedByLastEventDate() throws Exception {
        final String employees = "[\n" +
                "{\"name\": \"Pep\",\"lastEventDate\": \"1985-04-23\"},\n" +
                "{\"name\": \"Joana\",\"lastEventDate\": \"2015-08-15\"},\n" +
                "{\"name\": \"Nina\",\"lastEventDate\": \"2012-12-12\"},\n" +
                "{\"name\": \"Miquel\",\"lastEventDate\": \"1999-09-19\"},\n" +
                "{\"name\": \"Rosa\"}\n" +
                "]";
        given(employeeService.getEmployeesOrderedByLastEventDate()).willReturn(employees);
        mockMvc.perform(get("/api/employees")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();

        verify(employeeService, times(1)).getEmployeesOrderedByLastEventDate();
    }
}
