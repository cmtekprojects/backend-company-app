package com.company.service;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

    @InjectMocks
    private EmployeeService employeeService;

    @Test
    public void shouldReturnAllEmployeesOrderedByLastEventDate() {
        assertNotNull(employeeService.getEmployeesOrderedByLastEventDate());
    }
}
