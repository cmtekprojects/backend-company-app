package com.company.controller;

import com.company.app.model.EmployeeResponse;
import com.company.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@Api(value = "Employee data accessor")
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(final EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "employees")
    @ApiOperation(value = "Return all employees ordered by last event date", response = EmployeeResponse.class)
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Badrequest")})
    public ResponseEntity<EmployeeResponse> getAllEmployeesOrderedByLastEventDate() {
        final EmployeeResponse employeeResponse = EmployeeResponse.builder()
                .employees(employeeService.getEmployeesOrderedByLastEventDate())
                .build();
        return ResponseEntity.ok(employeeResponse);
    }

}
