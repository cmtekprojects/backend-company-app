package com.company.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

@Service
public class EmployeeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeService.class);
    private static final String EMPLOYEES_NODE = "employees";
    private static final String LAST_EVENT_DATE = "lastEventDate";

    public static final String EMPLOYEES = "{\"employees\": [\n" +
            "{\"name\": \"Pep\",\"lastEventDate\": \"1985-04-23\"},\n" +
            "{\"name\": \"Joana\",\"lastEventDate\": \"2015-08-15\"},\n" +
            "{\"name\": \"Nina\",\"lastEventDate\": \"2012-12-12\"},\n" +
            "{\"name\": \"Miquel\",\"lastEventDate\": \"1999-09-19\"},\n" +
            "{\"name\": \"Rosa\"}\n" +
            "]}";

    public String getEmployeesOrderedByLastEventDate() {
        try {
            final JsonNode node = new ObjectMapper().readTree(EMPLOYEES);
            final ArrayNode array = (ArrayNode) node.get(EMPLOYEES_NODE);
            final Iterator<JsonNode> iterator = array.elements();
            final List<JsonNode> list = new ArrayList<>();
            final List<JsonNode> listWithoutLastDate = new ArrayList<>();

            while (iterator.hasNext()) {
                final JsonNode nodeElement = iterator.next();
                if (nodeElement.has(LAST_EVENT_DATE)) {
                    list.add(nodeElement);
                } else {
                    listWithoutLastDate.add(nodeElement);
                }
            }

            list.sort(Comparator.comparing(o -> o.get(LAST_EVENT_DATE).asText()));
            list.addAll(listWithoutLastDate);

            return list.toString();
        } catch (final JsonMappingException e) {
            LOGGER.error("Error al mapear el json", e);
        } catch (final JsonProcessingException e) {
            LOGGER.error("Error al procesar el json", e);
        }
        return "";
    }
}
